1. Apakah perbedaan antara JSON dan XML?
JSON berbentuk dictionary, sedangkan XML menggunakan tags.
JSON tidak menyertakan newline dan indentasi untuk memperinda tampilan, sedangkan XML menggunakan newline dan indentation.
JSON merepresentasikan objects, sedangkan XML menggunakan bahasa markup dan tag untuk merepresentasikan data.
JSON tidak support namespaces, sedangkan XML support namespaces.
JSON tidak support comment, sedangkan XML support comments.
JSON hanya support encoding UTF-8, sedangkan XML support banyak encoding.
Sehingga JSON menjadi kurang secure, tidak seperti XML yang lebih secure.
JSON lebih mudah dibaca, tidak seperti XML yang lebih sulit untuk dibaca dan dimengerti.
JSON support array, XML tidak.

2. Apakah perbedaan antara HTML dan XML?
HTML menampilkan data yang di store pada XML.
XML berbentuk seperti isi file HTML (ada tags dsb.).
HTML adalah bahasa markup, XML adalah bahasa markup yang mengartikan bahasa markup lain.
HTML tidak case-sensitive, sedangkan XML case-sensitive.
Tags pada HTML sudah terdefinisi, sedangkan tags pada XML dapat didefinisikan sendiri.
Tags penutup terkadang tidak wajib di HTML, sedangkan pada XML tag penutup adalah hal yang wajib.
HTML statis, sedangkan XML dinamis.

Referensi
1. https://www.geeksforgeeks.org/difference-between-json-and-xml/
2. https://www.upgrad.com/blog/html-vs-xml/